var allowSubmit = false;

function onCaptchaFilled() {
    var element = document.getElementById("captchaError");
    element.classList.remove("is-visible");
    allowSubmit = true;
}

function onCaptchaExpired() {
    var element = document.getElementById("captchaError");
    element.classList.add("is-visible");
    allowSubmit = false;
}

function validateCaptcha(e) {
    if (allowSubmit) {
        return true;
    } else {
        var element = document.getElementById("captchaError");
        element.classList.add("is-visible");
        return false;
    }
}

function validateForm() {
    var nameError = document.getElementById("error-name");
    var emailError = document.getElementById("error-email");
    var messageError = document.getElementById("error-message");
    nameError.classList.remove("is-visible");
    emailError.classList.remove("is-visible");
    messageError.classList.remove("is-visible");

    var valid = true;

    var name = document.forms["contact-form"]["input-name"].value;
    if (name.trim() === "" || name.length < 5 && name.length > 255) {
        nameError.classList.add("is-visible");
        valid = false;
    }

    var emailReg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
    var email = document.forms["contact-form"]["input-email"].value;
    if (!emailReg.test(email) || email.trim() === "" || email.length < 5 || email.length > 254) {
        emailError.classList.add("is-visible");
        valid = false;
    }

    var message = document.forms["contact-form"]["input-message"].value;
    if (message.trim() === "" || message.length < 10 && message.length > 1000) {
        messageError.classList.add("is-visible");
        valid = false;
    }

    if (validateCaptcha() === false) {
        valid = false;
    }

    return valid;
}