package io.hoogland.portfolio.models;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ContactMessage {

    @Size(min = 2, max = 255)
    private String name;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @NotNull
    @NotEmpty
    private String message;
}
