package io.hoogland.portfolio.constants;

public class StaticRoutes {
    public final static String INDEX = "/";
    public final static String ABOUT = "/about";
    public final static String CV = "/cv";
    public final static String CONTACT = "/contact";
    public final static String LINKS = "/links";
}
