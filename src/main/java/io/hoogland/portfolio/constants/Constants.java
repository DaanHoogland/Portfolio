package io.hoogland.portfolio.constants;

public class Constants {

    public final static String EMAIL = "daan@hoogland.io";
    public final static String CONTENT = "<h3>Message info</h3><b>Sender:</b> %s<br><b>Email:</b> %s<br><b>Sent at:</b> %s<br><br><h3>Contents:</h3><pre>%s</pre>";

    public static final String GOOGLE_RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
}
