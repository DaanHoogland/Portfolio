package io.hoogland.portfolio.controllers;

import io.hoogland.portfolio.constants.Constants;
import io.hoogland.portfolio.constants.StaticRoutes;
import io.hoogland.portfolio.models.ContactMessage;
import io.hoogland.portfolio.recaptcha.RecaptchaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Slf4j
@Controller
public class ContactController {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private RecaptchaService recaptchaService;

    @GetMapping(StaticRoutes.CONTACT)
    public String getContact(ModelMap map) {
        map.addAttribute("contactMessage", new ContactMessage());
        return "contact";
    }

    @PostMapping(StaticRoutes.CONTACT)
    public String postContactForm(@Valid @ModelAttribute ContactMessage contactMessage,
                                  BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {

        String response = request.getParameter("g-recaptcha-response");
        String ip = request.getRemoteAddr();
        String captchaVerifyMessage = recaptchaService.verifyRecaptcha(ip, response);

        if (result.hasErrors()) {
            if (StringUtils.hasText(captchaVerifyMessage)) {
                result.reject("global");
            }
            return "contact";
        } else if (StringUtils.hasText(captchaVerifyMessage)) {
            result.reject("global");
            return "contact";
        }

        MimeMessagePreparator mailMessage = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setFrom("dress.server@gmail.com", "Portfolio Contact");
            message.setTo("daan@hoogland.io");
            message.setSubject("Portfolio | Message from: " + contactMessage.getName());
            message.setText(String.format(Constants.CONTENT, contactMessage.getName(), contactMessage.getEmail(),
                    OffsetDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("HH:mm 'on' dd-MM-yyyy")),
                    contactMessage.getMessage()), true);
        };

        try {
            emailSender.send(mailMessage);
        } catch (MailException mailException) {
            redirectAttributes.addAttribute("status", "error");
            return "contact";
        }

        redirectAttributes.addAttribute("status", "success");

        return String.format("redirect:%s", StaticRoutes.CONTACT);
    }
}
