package io.hoogland.portfolio.controllers;

import io.hoogland.portfolio.constants.StaticRoutes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping(StaticRoutes.INDEX)
    public String getIndex(ModelMap map) {
        return "index";
    }

    @GetMapping(StaticRoutes.ABOUT)
    public String getAbout(ModelMap map) {
        return "about";
    }

    @GetMapping(StaticRoutes.CV)
    public String getCv(ModelMap map) {
        return "cv";
    }

    @GetMapping(StaticRoutes.LINKS)
    public String getLinks(ModelMap map) {
        return "links";
    }
}
